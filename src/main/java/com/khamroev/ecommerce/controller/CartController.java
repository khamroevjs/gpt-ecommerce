package com.khamroev.ecommerce.controller;

import com.khamroev.ecommerce.Constants;
import com.khamroev.ecommerce.model.Cart;
import com.khamroev.ecommerce.model.CartItem;
import com.khamroev.ecommerce.model.Product;
import com.khamroev.ecommerce.model.User;
import com.khamroev.ecommerce.repository.CartRepository;
import com.khamroev.ecommerce.repository.ProductRepository;
import com.khamroev.ecommerce.repository.UserRepository;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/carts")
public class CartController {

    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    @Autowired
    public CartController(UserRepository userRepository, CartRepository cartRepository, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
    }

    @PostMapping("/add-product/{productId}")
    public Cart addToCart(@PathVariable long productId, HttpSession session) {
        UUID id = (UUID) session.getAttribute(Constants.USER_ID);
        Product product = productRepository.findById(productId).orElseThrow();
        if (id == null) {
            id = UUID.randomUUID();
            session.setAttribute(Constants.USER_ID, id);
            User user = userRepository.save(new User(id));
            Cart cart = new Cart();
            cart.setUser(user);
            cart.getCartItems().add(new CartItem(product, cart));
            cartRepository.save(cart);
            return cart;
        }
        Cart cart = cartRepository.findByUser_Id(id);
        cart.getCartItems().add(new CartItem(product, cart));
        cartRepository.save(cart);
        return cart;
    }

    @GetMapping("/{cartId}")
    public Cart getCart(@PathVariable long cartId) {
        return cartRepository.findById(cartId).orElseThrow();
    }
}

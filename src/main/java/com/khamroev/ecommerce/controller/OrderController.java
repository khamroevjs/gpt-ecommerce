package com.khamroev.ecommerce.controller;

import com.khamroev.ecommerce.Constants;
import com.khamroev.ecommerce.model.Cart;
import com.khamroev.ecommerce.model.Order;
import com.khamroev.ecommerce.model.OrderItem;
import com.khamroev.ecommerce.model.User;
import com.khamroev.ecommerce.repository.CartRepository;
import com.khamroev.ecommerce.repository.OrderRepository;
import com.khamroev.ecommerce.repository.UserRepository;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository, CartRepository cartRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;
    }

    @PostMapping("/place-order/{cardId}")
    public Order placeOrder(@PathVariable long cartId, HttpSession session) {
        Cart cart =  cartRepository.findById(cartId).orElseThrow();
        Order order = new Order();
        List<OrderItem> orderItems = cart.getCartItems().stream().map(x-> new OrderItem(x.getProduct(), order)).toList();
        order.setOrderItems(orderItems);
        UUID id = (UUID)session.getAttribute(Constants.USER_ID);
        User user = userRepository.findById(id).orElseThrow();
        order.setUser(user);
        return orderRepository.save(order);
    }

    @GetMapping("/{orderId}")
    public Order getOrder(@PathVariable Long orderId) {
        return orderRepository.findById(orderId).orElseThrow();
    }
}

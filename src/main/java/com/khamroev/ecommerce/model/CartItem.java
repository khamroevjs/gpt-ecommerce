package com.khamroev.ecommerce.model;

import jakarta.persistence.*;

@Entity
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Cart cart;

    public CartItem() {
    }

    public CartItem(Product product, Cart cart) {
        this.product = product;
        this.cart = cart;
    }

    public CartItem(long id, Product product, Cart cart) {
        this.id = id;
        this.product = product;
        this.cart = cart;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
